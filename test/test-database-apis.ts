#!/usr/bin/env node
//
// Current hacky running instructions:
// git submodule update --init --recursive
// cd vendor/couchbase-lite-core/build_cmake
// ./build_unix.sh
// cd ../../../../
// npm install
// node-gyp configure build
// LD_LIBRARY_PATH=./vendor/couchbase-lite-core/build_cmake/unix npm test

const fs = require("fs");

import { libLiteCore } from "../index";

const rimraf = require("rimraf").sync;

const isBuffer = require("is-buffer");

// TODO: Use unique filenames under /tmp.

function cleanTestDatabases() {
  rimraf("./foobar");
  rimraf("./foobar-2");
  rimraf("./foobar-3");
  rimraf("./foobar-4");
  rimraf("./foobar-copy");
}

// NOTE! These tests are executed by `npm test` sequentially (--runInBand) because there is a time order.

test("Test build info", () => {
  expect(
    libLiteCore.c4_getBuildInfo().indexOf("built from")
  ).toBeGreaterThanOrEqual(0);
});

test("Test database flag constant existence", () => {
  expect(libLiteCore.C4DatabaseFlags).toBeTruthy();
  expect(libLiteCore.C4DatabaseFlags.kC4DB_Create).toEqual(1);
  expect(libLiteCore.C4DatabaseFlags.kC4DB_ReadOnly).toEqual(2);
  expect(libLiteCore.C4DatabaseFlags.kC4DB_AutoCompact).toEqual(4);
  expect(libLiteCore.C4DatabaseFlags.kC4DB_SharedKeys).toEqual(16);
  expect(libLiteCore.C4DatabaseFlags.kC4DB_NoUpgrade).toEqual(32);
  expect(libLiteCore.C4DatabaseFlags.kC4DB_NonObservable).toEqual(64);
});

test("Test logging level handling", () => {
  expect(libLiteCore.C4LogDomain.kC4DefaultLog).toBeTruthy();
  expect(libLiteCore.C4LogDomain.kC4DatabaseLog).toBeTruthy();
  expect(libLiteCore.C4LogDomain.kC4QueryLog).toBeTruthy();
  expect(libLiteCore.C4LogDomain.kC4SyncLog).toBeTruthy();
  expect(libLiteCore.C4LogDomain.kC4WebSocketLog).toBeTruthy();

  expect(libLiteCore.C4LogLevel.kC4LogDebug).toEqual(0);
  expect(libLiteCore.C4LogLevel.kC4LogVerbose).toEqual(1);
  expect(libLiteCore.C4LogLevel.kC4LogInfo).toEqual(2);
  expect(libLiteCore.C4LogLevel.kC4LogWarning).toEqual(3);
  expect(libLiteCore.C4LogLevel.kC4LogError).toEqual(4);
  expect(libLiteCore.C4LogLevel.kC4LogNone).toEqual(5);

  // may seem pointless to test concrete constant values that may be subject to change,
  // but done in part to test the encoding/decoding of the externals involved underneath
  // ... and multiple bugs with the boilerplate involved in mapping these constants from the original symbols were found in the process of writing this stuff.
  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.C4LogDomain.kC4DefaultLog)
  ).toEqual("");
  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.C4LogDomain.kC4DatabaseLog)
  ).toEqual("DB");
  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.C4LogDomain.kC4QueryLog)
  ).toEqual("Query");
  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.C4LogDomain.kC4SyncLog)
  ).toEqual("Sync");
  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.C4LogDomain.kC4WebSocketLog)
  ).toEqual("WS");

  expect(
    libLiteCore.c4log_getDomainName(libLiteCore.c4log_getDomain("DB", false))
  ).toEqual("DB");

  expect(
    libLiteCore.c4log_getLevel(libLiteCore.C4LogDomain.kC4DefaultLog)
  ).toEqual(libLiteCore.C4LogLevel.kC4LogInfo);
  libLiteCore.c4log_setLevel(
    libLiteCore.C4LogDomain.kC4DefaultLog,
    libLiteCore.C4LogLevel.kC4LogDebug
  );
});

const config = {
  flags: libLiteCore.C4DatabaseFlags.kC4DB_Create,
  storageEngine: libLiteCore.kC4SQLiteStorageEngine,
  versioning: libLiteCore.C4DocumentVersioning.kC4RevisionTrees,
  encryptionKey: {
    algorithm: libLiteCore.C4EncryptionAlgorithm.kC4EncryptionNone,
    bytes: Buffer.alloc(32)
  }
};

test("Test a sequence of opening, copying, closing, opening again, deleting a database", () => {
  let db = libLiteCore.c4db_open("./foobar", config);
  const docCount = libLiteCore.c4db_getDocumentCount(db);
  expect(docCount).toEqual(0);
  expect(libLiteCore.c4db_close(db)).toBeTruthy();

  expect(fs.existsSync("./foobar")).toBeTruthy();

  let oldDB = db;
  db = libLiteCore.c4db_openAgain(db); // TODO: Test the state of the old db (it is probably in some kind of stale state that should be warned against)?
  expect(db).toBeTruthy();
  expect(db === oldDB).toBeFalsy();
  oldDB = null;
  // TODO: Fix these -- failing with "Is a directory"
  // expect(libLiteCore.c4db_copy("./foobar", "./foobar-copy", config)).toBeTruthy();
  // expect(fs.existsSync("./foobar-copy")).toBeTruthy();

  expect(libLiteCore.c4db_delete(db)).toBeTruthy();
  db = null;
});

test("Test transaction APIs", () => {
  const db2 = libLiteCore.c4db_open("./foobar-2", config);

  expect(libLiteCore.c4db_isInTransaction(db2)).toBeFalsy();
  expect(libLiteCore.c4db_beginTransaction(db2)).toBeTruthy();
  expect(libLiteCore.c4db_isInTransaction(db2)).toBeTruthy();
  expect(libLiteCore.c4db_endTransaction(db2, true)).toBeTruthy();

  try {
    libLiteCore.c4raw_get(
      db2,
      libLiteCore.kC4InfoStore,
      "doc_that_does_not_exist"
    );
    expect(false).toBeTruthy(); // this should be unreachable. Expecting to throw didn't work for sam
  } catch (e) {
    console.log(
      `Exception occurred when attempting to get a document that does not exist:\n${
        e
      }`
    );
  }

  expect(libLiteCore.c4db_beginTransaction(db2)).toBeTruthy();
  expect(
    libLiteCore.c4raw_put(
      db2,
      libLiteCore.kC4InfoStore,
      "doc_that_will_exist",
      "meta",
      '{"name":007}'
    )
  );
  expect(libLiteCore.c4db_endTransaction(db2, true)).toBeTruthy();

  const doc = libLiteCore.c4raw_get(
    db2,
    libLiteCore.kC4InfoStore,
    "doc_that_will_exist"
  );
  expect(doc.key()).toEqual("doc_that_will_exist");
  expect(doc.meta()).toEqual("meta");
  expect(doc.body()).toEqual('{"name":007}');

  expect(libLiteCore.c4db_close(db2)).toBeTruthy();
  expect(libLiteCore.c4db_deleteAtPath("./foobar-2")).toBeTruthy();
});

test("Test miscellaneous APIs: path, sequence, doc expiration, max rev tree depth, compacting,", () => {
  const db3 = libLiteCore.c4db_open("./foobar-3", config);
  expect(db3).toBeTruthy();
  expect(libLiteCore.c4db_getPath(db3)).toEqual("./foobar-3/");
  expect(libLiteCore.c4db_getLastSequence(db3)).toEqual(0);
  expect(libLiteCore.c4db_nextDocExpiration(db3)).toEqual(0);
  expect(libLiteCore.c4db_getMaxRevTreeDepth(db3)).toEqual(20); // this may change in different releases?
  libLiteCore.c4db_setMaxRevTreeDepth(db3, 15);
  expect(libLiteCore.c4db_getMaxRevTreeDepth(db3)).toEqual(15);
  expect(libLiteCore.c4db_compact(db3)).toBeTruthy();
});

test("Test the public & private UUIDs being available", () => {
  const db4 = libLiteCore.c4db_open("./foobar-4", config);
  const uuids: any = libLiteCore.c4db_getUUIDs(db4);
  expect(uuids).toHaveProperty("privateUUID");
  expect(uuids).toHaveProperty("publicUUID");
  expect(isBuffer(uuids.privateUUID.bytes)).toBeTruthy();
  expect(isBuffer(uuids.publicUUID.bytes)).toBeTruthy();
  expect(uuids.privateUUID.bytes === uuids.publicUUID.bytes).toBeFalsy();
});

// NOTE! This test needs to be run with --runInBand --expose-gc
test("Test that there are no leaks of C4 objects.", () => {
  if (global.gc) {
    console.log("Forcing a collection...");
    global.gc();
  } else {
    console.warn(
      "WARNING! global.gc not available (tests run without --expose-gc?)."
    );
  }

  libLiteCore.c4_shutdown();
  libLiteCore.c4_dumpInstances();
  console.log(`Object count: ${libLiteCore.c4_getObjectCount()}`);
  expect(libLiteCore.c4_getObjectCount()).toEqual(0);
});

afterAll(() => {
  cleanTestDatabases();
});
