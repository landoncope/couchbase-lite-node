{
    "targets": [
        {
            "target_name": "fleece",
            "sources": [ "./C/NModule.c", "./C/NFleece.c" ],
            "include_dirs": [ "<(module_root_dir)/vendor/couchbase-lite-core/vendor/fleece/Fleece" ],
            "link_settings": {
                "libraries": ["-lLiteCore"],
                "ldflags": ["-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/unix"]
            },
            "cflags": [ "-Wno-attributes", "-Wno-unknown-pragmas" ],
            "cflags_cc": [ "-Wno-attributes", "-Wno-unknown-pragmas", "-std=c++11" ],
            "ldflags": ["-LLiteCore"],
            "cflags!": [ "-fno-exceptions" ],
            "cflags_cc!": [ "-fno-exceptions" ],
            "conditions": [ ["OS==\"mac\"", {
                "libraries": [ "-lLiteCore" ],
                "include_dirs": [ "<(module_root_dir)/vendor/couchbase-lite-core/vendor/fleece/Fleece" ],
                "ldflags": [
                    "-Wl,-rpath,<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos",
                    "-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos"
                ],
                "xcode_settings": {
                    "GCC_ENABLE_CPP_EXCEPTIONS": "YES",
                    "GCC_ENABLE_CPP_RTTI": "NO",
                    "INSTALL_PATH": "@rpath",
                    "LD_DYLIB_INSTALL_NAME": "",
                    "OTHER_LDFLAGS": [
                        "-Wl,-search_paths_first",
                        "-Wl,-rpath,<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos",
                        "-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos"
                    ]
                }
                }] ]
        },
        {
            "target_name": "couchbaselite",
            "sources": [ "./C/CBNModule.c", 
                         "./C/CBNBase.c", 
                         "./C/CBNDatabase.c",
                         "./C/CBNDocument.c", 
                         "./C/CBNUtilities.c" ],
            "include_dirs": [ "<(module_root_dir)/vendor/couchbase-lite-core/C/include", 
                              "<(module_root_dir)/vendor/couchbase-lite-core/vendor/fleece/Fleece" ],
            "link_settings": {
                "libraries": ["-lLiteCore"],
                "ldflags": ["-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/unix"]
            },
            "cflags": [ "-Wno-attributes", "-Wno-unknown-pragmas" ],
            "cflags_cc": [ "-Wno-attributes", "-Wno-unknown-pragmas", "-std=c++11" ],
            "ldflags": ["-LLiteCore"],
            "cflags!": [ "-fno-exceptions" ],
            "cflags_cc!": [ "-fno-exceptions" ],
            "conditions": [ ["OS==\"mac\"", {
                "libraries": [ "-lLiteCore" ],
                "include_dirs": [ "<(module_root_dir)/vendor/couchbase-lite-core/C/include", 
                                  "<(module_root_dir)/vendor/couchbase-lite-core/vendor/fleece/Fleece" ],
                "ldflags": [
                    "-Wl,-rpath,<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos",
                    "-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos"
                ],
                "xcode_settings": {
                    "GCC_ENABLE_CPP_EXCEPTIONS": "YES",
                    "GCC_ENABLE_CPP_RTTI": "NO",
                    "INSTALL_PATH": "@rpath",
                    "LD_DYLIB_INSTALL_NAME": "",
                    "OTHER_LDFLAGS": [
                        "-Wl,-search_paths_first",
                        "-Wl,-rpath,<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos",
                        "-L<(module_root_dir)/vendor/couchbase-lite-core/build_cmake/macos"
                    ]
                }
                }] ]
        }
    ]
}