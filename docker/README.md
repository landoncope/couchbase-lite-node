The Dockerfile in this folder is the gitlab.com/mpapp-public/couchbase-lite-node image.

To update the Docker image:

./build-push-img.sh

NOTE! If your account has a two-factor authentication enabled, you need to authenticate with registry.gitlab.com not with your usual GitLab password, but with a personal authorization token that you can create under [User Settings > Access Tokens](https://gitlab.com/profile/personal_access_tokens).
